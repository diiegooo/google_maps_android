package com.example.mymaps;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener {


    EditText latitud, longitud;

    GoogleMap mapa;
    Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        latitud = findViewById(R.id.latitud);
        longitud = findViewById(R.id.longitud);

        Button updateLocationButton = findViewById(R.id.boton);
        updateLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateLocation();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);
    }

    private void updateLocation() {
        if (mapa != null) {

            double lat = Double.parseDouble(latitud.getText().toString());
            double lon = Double.parseDouble(longitud.getText().toString());


            LatLng newLocation = new LatLng(lat, lon);


            mapa.moveCamera(CameraUpdateFactory.newLatLng(newLocation));


            if (marker != null) {
                marker.remove();
            }


            marker = mapa.addMarker(new MarkerOptions().position(newLocation).title("Nueva Ubicación"));
        }
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mapa = googleMap;
        this.mapa.setOnMapClickListener(this);
        this.mapa.setOnMapLongClickListener(this);

        LatLng inicio = new LatLng(40.4002196,-3.6686807);
        mapa.addMarker(new MarkerOptions().position(inicio).title("ITEP"));
        mapa.moveCamera(CameraUpdateFactory.newLatLng(inicio));

    }

    @Override
    public void onMapClick(@NonNull LatLng latLng) {
        latitud.setText(String.valueOf(latLng.latitude));
        longitud.setText(String.valueOf(latLng.longitude));

        mapa.clear();
        LatLng aa = new LatLng(latLng.latitude, latLng.longitude);
        mapa.addMarker(new MarkerOptions().position(aa).title(""));
        mapa.moveCamera(CameraUpdateFactory.newLatLng(aa));
    }

    @Override
    public void onMapLongClick(@NonNull LatLng latLng) {
        latitud.setText(String.valueOf(latLng.latitude));
        longitud.setText(String.valueOf(latLng.longitude));

        mapa.clear();
        LatLng aa = new LatLng(latLng.latitude, latLng.longitude);
        mapa.addMarker(new MarkerOptions().position(aa).title(""));
        mapa.moveCamera(CameraUpdateFactory.newLatLng(aa));
    }
}